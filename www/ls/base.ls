container = d3.select ig.containers.base
zapasy = d3.tsv.parse ig.data.zapasy, (row) ->
  row.score1 = parseInt row.score1, 10
  row.score2 = parseInt row.score2, 10
  [d, m, y] = row.date.split "." .map parseInt _, 10
  [H, i] = row.time.split ":" .map parseInt _, 10
  row.date = new Date!
    ..setTime 0
    ..setDate d
    ..setMonth m - 1
    ..setFullYear y
    ..setHours H
    ..setMinutes i
  if isNaN row.score1
    row.score1 = 0
    row.score2 = 0
    row.isDecided = no
    row.isCountable = no
    row.increment = (side, amount) ->
      if side == 1
        row.score1 += amount
        if row.score1 < 0 then row.score1 = 0
        row.element.select ".score1" .html row.score1
      else
        row.score2 += amount
        if row.score2 < 0 then row.score2 = 0
        row.element.select ".score2" .html row.score2
      row.isCountable = yes

      redrawResults!
  else
    row.isCountable = yes
    row.isDecided = yes
  row

nameToFlag =
  "Island" : "isl"
  "Česko" : "cze"
  "Nizozemsko" : "nld"
  "Turecko" : "tur"
  "Lotyšsko" : "lat"
  "Kazachstán" : "kaz"
months = <[ledna února března dubna května června července srpna září října listopadu prosince]>
getComparableDate = (date) ->
  "#{date.getFullYear!}-#{date.getMonth!}-#{date.getDate!}"
today = getComparableDate new Date!
zapasyContainer = container.append \ul
  ..attr \class \zapasy
  ..selectAll \li .data zapasy .enter!append \li
    ..each -> it.element = d3.select @
    ..classed \decided (.isDecided)
    ..classed \not-countable (.isCountable == no)
    ..append \div
      ..attr \class \names
      ..append \div
        ..attr \class "name name1"
        ..append \span
          ..html (.player1)
        ..append \img
          ..attr \src -> "https://samizdat.cz/data/fotbal-kvalifikace/www/img/#{nameToFlag[it.player1]}.png"
      ..append \div
        ..attr \class "name name2"
        ..append \span
          ..html (.player2)
        ..append \img
          ..attr \src -> "https://samizdat.cz/data/fotbal-kvalifikace/www/img/#{nameToFlag[it.player2]}.png"
    ..append \div
      ..attr \class \date
      ..html -> "#{it.date.getDate!}. #{months[it.date.getMonth!]} #{it.date.getFullYear!}"
    ..append \div
      ..attr \class \scores
      ..append \span
        ..attr \class "score score1"
        ..html (.score1)
      ..append \span
        ..attr \class "score score2"
        ..html (.score2)
      ..filter (.isDecided == no)
        ..append \div
          ..attr \class "score-modifier score-modifier1"
          ..append \a
            ..attr \href \#
            ..html "+"
            ..on \click ->
              d3.event.preventDefault!
              it.increment 1, 1
          ..append \a
            ..attr \href \#
            ..html "-"
            ..on \click ->
              d3.event.preventDefault!
              it.increment 1, -1
        ..append \div
          ..attr \class "score-modifier score-modifier2"
          ..append \a
            ..attr \href \#
            ..html "+"
            ..on \click ->
              d3.event.preventDefault!
              it.increment 2, 1
          ..append \a
            ..attr \href \#
            ..html "-"
            ..on \click ->
              d3.event.preventDefault!
              it.increment 2, -1
    ..filter (-> (getComparableDate it.date) is today)
      ..classed \today yes
      ..append \div
        ..attr \class \today
        ..html "Dnes"
zapasyElements = container.selectAll \li
tableHeight = null
zapasyHeight = null
containerOffset = null
redrawResults = ->
  zapasyElements.classed \not-countable (.isCountable == no)
  tymy = []
  tymyAssoc = {}
  zapasyAssoc = {}
  for zapas in zapasy
    for name in [zapas.player1, zapas.player2]
      if tymyAssoc[name] is void
        tym = {name, points: 0, goals: 0, matches: []}
        tymy.push tym
        tymyAssoc[name] = tym
      tym.matches.push zapas
    continue unless zapas.isCountable
    zapasyAssoc[[zapas.player1, zapas.player2].join "-"] = zapas
    tymyAssoc[zapas.player1].goals += zapas.score1
    tymyAssoc[zapas.player2].goals += zapas.score2
    if zapas.score1 > zapas.score2
      tymyAssoc[zapas.player1].points += 3
    else if zapas.score1 < zapas.score2
      tymyAssoc[zapas.player2].points += 3
    else
      tymyAssoc[zapas.player1].points += 1
      tymyAssoc[zapas.player2].points += 1
  tymy.sort (a, b) ->
    | b.points - a.points => that
    | b.goals - a.goals => that
    | otherwise
      zapas1 = zapasyAssoc[[zapas.player1, zapas.player2].join "-"]
      zapas2 = zapasyAssoc[[zapas.player2, zapas.player1].join "-"]
      goals1 = 0
      goals2 = 0
      if zapas1
        goals1 += zapas1.score1
        goals2 += zapas1.score2
      if zapas2
        goals1 += zapas2.score2
        goals2 += zapas2.score1
      if goals2 - goals1
        that
      else
        if zapas1
          goals2 += zaspas1.score2
        if zapas2
          goals1 += zapas2.score2
        if goals2 - goals1
          that
        else
          a.undecided = yes
          b.undecided = yes
          0
  for tym, index in tymy
    tym.index = index
  tableBody.selectAll \tr .data(tymy, (.name))
    ..enter!
      ..append \tr
        ..append \td
          ..attr \class \order
        ..append \th
          ..append \img
            ..attr \src -> "https://samizdat.cz/data/fotbal-kvalifikace/www/img/#{nameToFlag[it.name]}.png"
          ..append \span
            ..html (.name)
        ..append \td
          ..attr \class \points
    ..style \top (d, i) -> "#{i * 32}px"
    ..attr \data-index (.index)
    ..select \td.order
      ..html -> "#{it.index + 1}."
    ..select \td.points
      ..html ->
        plural =
          | 2 <= it.points < 5 => "body"
          | it.points == 1 => "bod"
          | _ => "bodů"
        "#{it.points} #plural"

  tableHeight := tymy.length * 32
  zapasyHeight := zapasyContainer.node!clientHeight
  containerOffset := ig.utils.offset container.node! .top


tableContainer = container.append \table
tableBody = tableContainer.append \tbody
redrawResults!

window.addEventListener \scroll ->
  maxTop = zapasyHeight - tableHeight
  scrollTop = (document.body.scrollTop || document.documentElement.scrollTop)
  top = scrollTop - containerOffset
  top = 0 if top < 0
  top = maxTop if top > maxTop
  tableContainer.style \top "#{top}px"
